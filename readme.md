#Jest setup for ES2015

A simple setup for Jest testing using ES2015 with babel.
Based on the Jest [Getting started example](https://github.com/facebook/jest/tree/master/examples/getting_started).

To install;

`npm i`

To run the test;

`npm run test`

Babel presets are included in `package.json` file.