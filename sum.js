function sum(a, b) {
  return a + b;
}
//es5 export
//module.exports = sum;

//es6 export
export default sum;