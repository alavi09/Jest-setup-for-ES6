jest.unmock('../sum'); // unmock to use the actual implementation of sum
// import function
import sum from '../sum'

describe('sum', () => {
  it('adds 1 + 2 to equal 3', () => {

    //es5 implementation   
    //const sum = require('../sum');

    expect(sum(1, 2)).toBe(3);
  });
});